FROM fluent/fluentd:v1.5-1

USER root

RUN apk add --no-cache --update --virtual .build-deps \
        sudo build-base ruby-dev \
 && sudo gem install ruby-kafka  --version 0.4.3 \
 && sudo gem install fluent-plugin-kafka  --version 0.6.6 \
 && sudo gem install fluent-plugin-multi-format-parser \
 && sudo gem install fluent-plugin-flowcounter \
 && sudo gem install fluent-plugin-rewrite-tag-filter \
 && sudo gem install fluent-plugin-filter_typecast \
 && sudo gem install fluent-plugin-flatten \
 && sudo gem install fluent-plugin-extract_query_params \
 && sudo gem install fluent-plugin-route \
 && sudo gem install fluent-plugin-rewrite-tag-filter \
 && sudo gem sources --clear-all \
 && apk del .build-deps \
 && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

USER fluent